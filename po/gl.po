# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the xfce-panel-plugins.xfce4-time-out-plugin package.
# 
# Translators:
# Daniel Muñiz Fontoira <dani@damufo.eu>, 2018-2021
# Leandro Regueiro <leandro.regueiro@gmail.com>, 2008-2010
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2024-04-07 00:56+0200\n"
"PO-Revision-Date: 2013-07-03 19:23+0000\n"
"Last-Translator: Daniel Muñiz Fontoira <dani@damufo.eu>, 2018-2021\n"
"Language-Team: Galician (http://app.transifex.com/xfce/xfce-panel-plugins/language/gl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: gl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Create label for displaying the remaining time until the next break
#: panel-plugin/time-out.c:223
msgid "Inactive"
msgstr "Inactivo"

#. Create menu item for taking an instant break
#: panel-plugin/time-out.c:277
msgid "Take a break"
msgstr "Tome un descanso"

#. Create menu item for resetting the timer
#: panel-plugin/time-out.c:285
msgid "Reset timer"
msgstr "Restablecer o temporizador"

#. Create menu item for enabling/disabling the countdown
#: panel-plugin/time-out.c:293
msgid "Enabled"
msgstr "Activado"

#. Update tooltips
#: panel-plugin/time-out.c:357
msgid "Paused"
msgstr "En pausa"

#: panel-plugin/time-out.c:439
msgid ""
"Xfce Panel plugin for taking a break from computer work every now and then."
msgstr "O engadido de panel do Xfce para tomar un descanso no traballo co computador de cando en vez."

#: panel-plugin/time-out.c:444
msgid "translator-credits"
msgstr "Leandro Regueiro <leandro.regueiro@gmail.com>, 2008, 2009, 2010."

#: panel-plugin/time-out.c:478 panel-plugin/time-out.c:483
#: panel-plugin/time-out-lock-screen.c:333
#: panel-plugin/xfce4-time-out-plugin.desktop.in:5
msgid "Time Out"
msgstr "Tempo de espera"

#: panel-plugin/time-out.c:480
msgid "_Close"
msgstr "_Pechar"

#. Create time settings section
#: panel-plugin/time-out.c:500
msgid "Time settings"
msgstr "Configuración de tempo"

#. Create the labels for the minutes and seconds spins
#: panel-plugin/time-out.c:513
msgid "Minutes"
msgstr "Minutos"

#: panel-plugin/time-out.c:518
msgid "Seconds"
msgstr "Segundos"

#. Create break countdown time label
#: panel-plugin/time-out.c:524
msgid "Time between breaks:"
msgstr "Tempo entre descansos:"

#. Create lock countdown time label
#: panel-plugin/time-out.c:550
msgid "Break length:"
msgstr "Duración do descanso:"

#. Create postpone countdown time label
#: panel-plugin/time-out.c:571
msgid "Postpone length:"
msgstr "Duración da posposición:"

#. Create behaviour section
#: panel-plugin/time-out.c:592
msgid "Behaviour"
msgstr "Comportamento"

#. Create postpone check button
#: panel-plugin/time-out.c:603
msgid "Allow postpone"
msgstr "Permitir pospoñer"

#. Create resume check button
#: panel-plugin/time-out.c:610
msgid "Resume automatically"
msgstr "Continuar automaticamente"

#. Create appearance section
#: panel-plugin/time-out.c:617
msgid "Appearance"
msgstr "Aparencia"

#. Create note label
#: panel-plugin/time-out.c:628
msgid "Note: Icon and time cannot be hidden simultaneously."
msgstr "Nota: a icona e o tempo non se poden agochar simultaneamente."

#. Create display icon check button
#: panel-plugin/time-out.c:633
msgid "Display icon"
msgstr "Amosar unha icona"

#. Create display time check button
#: panel-plugin/time-out.c:640
msgid "Display remaining time in the panel"
msgstr "Amosar o tempo restante no panel"

#. Create display hours check button
#: panel-plugin/time-out.c:647
msgid "Display hours"
msgstr "Amosar as horas"

#. Create display seconds check button
#: panel-plugin/time-out.c:654
msgid "Display seconds"
msgstr "Amosar os segundos"

#: panel-plugin/time-out.c:1082
msgid "Failed to lock screen"
msgstr "Produciuse un erro ao bloquear a pantalla"

#. Create postpone button
#: panel-plugin/time-out-lock-screen.c:263
msgid "_Postpone"
msgstr "_Pospor"

#. Create lock button
#: panel-plugin/time-out-lock-screen.c:269
msgid "_Lock"
msgstr "_Bloquear"

#. Create resume button
#: panel-plugin/time-out-lock-screen.c:275
msgid "_Resume"
msgstr "_Continuar"

#: panel-plugin/time-out-lock-screen.c:332
msgid "Failed to grab input for Time Out lock screen"
msgstr "Produciuse un fallo ao obter a entrada para a pantalla de bloqueo de tempo de espera"

#: panel-plugin/time-out-countdown.c:390
msgid "The break is over."
msgstr "Rematou o descanso."

#. Hours:minutes:seconds
#: panel-plugin/time-out-countdown.c:412
#, c-format
msgid "%02d:%02d:%02d"
msgstr "%02d:%02d:%02d"

#. Hours:minutes
#. Minutes:seconds
#: panel-plugin/time-out-countdown.c:417 panel-plugin/time-out-countdown.c:425
#, c-format
msgid "%02d:%02d"
msgstr "%02d:%02d"

#: panel-plugin/time-out-countdown.c:437
#, c-format
msgid "%d hour"
msgid_plural "%d hours"
msgstr[0] "%d hora"
msgstr[1] "%d horas"

#: panel-plugin/time-out-countdown.c:438 panel-plugin/time-out-countdown.c:494
#: panel-plugin/time-out-countdown.c:507 panel-plugin/time-out-countdown.c:541
#, c-format
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] "%d minuto"
msgstr[1] "%d minutos"

#: panel-plugin/time-out-countdown.c:439
#, c-format
msgid "%d second"
msgid_plural "%d seconds"
msgstr[0] "%d segundo"
msgstr[1] "%d segundos"

#. Translators: this is %s seconds/minutes/hours
#: panel-plugin/time-out-countdown.c:450 panel-plugin/time-out-countdown.c:455
#: panel-plugin/time-out-countdown.c:466 panel-plugin/time-out-countdown.c:489
#: panel-plugin/time-out-countdown.c:495 panel-plugin/time-out-countdown.c:522
#: panel-plugin/time-out-countdown.c:527 panel-plugin/time-out-countdown.c:536
#: panel-plugin/time-out-countdown.c:542
#, c-format
msgid "Time left: %s"
msgstr "Tempo restante: %s"

#. Translators: this is %s minutes, %s seconds
#. Translators: this is %s hours %s seconds
#. Translators: this is %s hours, %s minutes
#. Translators: this is %s minutes %s seconds
#: panel-plugin/time-out-countdown.c:458 panel-plugin/time-out-countdown.c:470
#: panel-plugin/time-out-countdown.c:477 panel-plugin/time-out-countdown.c:502
#: panel-plugin/time-out-countdown.c:509 panel-plugin/time-out-countdown.c:530
#, c-format
msgid "Time left: %s %s"
msgstr "Tempo restante: %s e %s"

#. Translators: this is %s hours, %s minutes, %s seconds
#: panel-plugin/time-out-countdown.c:480
#, c-format
msgid "Time left: %s %s %s"
msgstr "Tempo restante: %s %s e %s"

#: panel-plugin/xfce4-time-out-plugin.desktop.in:6
msgid "Automatically controlled time outs and breaks"
msgstr "Tempos de espera e descansos controlados automaticamente"
